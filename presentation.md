title: Katuma i Open Food Network
layout: true

---

class: impact

# {{title}}
### Gestió de Sistemes d'Informació

---

## Pau Pérez

.thin.small[@prez_pau]

.thin.small[membre de Coopdevs]<br>
.thin.small[impulsant Katuma]<br>
.thin.small[al Core Team d'Open Food Network]

---

class: impact
background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/negoci.jpg)

## Negoci

---

### Marc teòric

Agroecologia

Consum responsable

Circuits curts de comercialització

Economia social i solidària

Cooperativisme

---

class: only-quote

### Agroecologia

> “El moviment agroecològic té una visió molt més holística i també mira les condicions socials dels pagesos, els quilòmetres que ha viatjat el producte, l’embalatge, la integració en el territori o la biodiversitat” 
.author[Ricard Espelt, La Directa]

---

class: impact

### Cooperativisme de consum

---

### Principis cooperatius

Adhesió voluntària

Gestió democrática

Participació econòmica

Autonomia i independència

Educació i formació

Intercooperació

Interès per la comunitat

---

### Productors locals agroecològics

Molt més que ecològics

Poc volum

Distribueixen els seus productes

Voluntat de transformació

Relació directa amb el client

---

### Grups de consum

Compra col·lectiva

Treball voluntari

Suport als productors

Comandes setmanals de fruita i verdura

Petits i mitjans

Relació directa amb el proveïdor

---

background-image: url(img/network_analysis.png)

### <span style="color: black"> Relació entre els actors</span>

---

### Anàlisi del sector

Zènit dels grups de consum

---

### Anàlisi del sector

Esforç de gestió

Incidències en les comandes

Oferta eco molt accessible

Poca flexibilitat

Sovint "elitista"

Aparició de nous models

---

class: only-quote

### Anàlisi del sector

> El canvi d'escala: un revulsiu per a la sostenibilitat del cooperativisme agroecològic?

.author[Aresta, 2017]

---

class: impact
background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/katuma.jpg)

## Katuma

---

class: impact

Resposta tech al canvi d'escala

---

### Estat actual

N solucions no mantingudes

Software self-hosted

Fulls de càlcul

---

class: impact

Superar contres de sol·lucions establertes

---

### Contres

Dependència d'un informàtic (SPOF)

Instal·lació

Costos d'infraestructura (self-hosted)

Costos de (no) manteniment del software

Fragilitat de fulls de càlcul

---

### Aposta

Marketplace Saas per a la gestió de comandes

---

class: only-quote

### Futur

> "La distribució és el gran problema que hem de resoldre"
.author[Qualsevol productor agroecològic]

---

### Cronologia

2012: Katuma -> 2016: Prototip al 40% -> 2017: Primer grup i productor ->

2018: Matchfunding, H2020, Katuma SCCL -> 2019: SCCL Operativa i autònoma

---

### Trets distintius

Platform Coop

Mirada global

Sobirania tecnològica

Eina àgil

Sense ànim de lucre

---

### Hipòtesi

Millora en eficiència => Reducció de costos => Sostenibilitat del sector


X€/mes / N socis = quota de soci; on X tendeix a ser fix

---

class: impact
background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/openfoodnetwork.png)

## Open Food Network

---

class: impact

Better food systems

![](img/better_food_systems.png)

---

class: impact

Improve resilience of short food supply chains

![](img/supply_chain.png)

---

### Estat actual

High transaction costs

Burnout due to poor IT systems

Reinventing the wheel

Lack of resource optimization

Lack of information transparency

---

class: impact

90% of the needs are shared
throughout the world

---

class: impact

background-image: url(img/ofn_map.png)

### .dark[International network of local projects]

---

### Aposta

software & know-how as a common

---

### Aposta

A distributed marketplace for food enterprises

---

### Tech

Spree e-commerce + The Network + Custom shopfront

---

### Tech

Ruby on Rails + PostgreSQL + AngularJS

---

### Tech

Isolated OFN instances

---

### Tech

Server = Nginx + Unicorn + PostgreSQL

---

### Tech

ansible scripts for provisioning
and deployment

---

### Trets distintius

B2B

Single € pot

No prescriptive

No legal entity

---

### Hipòtesi

More efficient enterprises => growth => sustainability

Y$/year / M instances = local contribution; where Y tends to be fixed

---

### Cronologia

2012: Open Food Foundation -> 2013: VicHealth, Open Food Network Beta ->

201x: UK, South Africa, Canada -> 201x: OFN Scandinavia ->

201x: Non-English speaking areas

---

class: impact

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/australia.jpg)

## Relació entre Katuma i OFN

---

class: impact

background-image: url(img/katuma_map.png)

.text-shadow[Instància ibèrica d'OFN]

---

class: impact

background-image: url(img/commons_levels.jpg)

3 nivells de comú

---

background-image: url(img/relacio_ofn_katuma.jpg)

---

class: impact

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/information_systems.jpg)

## Sistemes d'Informació

---

### Sistemes transaccionals

Productors

Consumidors

Logística

ERP

CRM*

---

### Sistemes d'ajut a la presa de decisions

BI

---

### Altres

Suport i resolució d'incidències

---

class: impact

## Òrgans

---

background-image: url(img/organs.jpg)

---

class: impact

## CTO

---

background-image: url(img/complet.jpg)
background-size: contain

---

### Katuma

Manteniment d'infaestructura més enllà de l'e-commerce

---

### Open Food Network

Tasques pròpies d'un Saas

Lideratge del Core Team

---

class: impact

## CIO

---

Anàlisi en termes d'economia procomú

---

background-image: url(img/complet.jpg)
background-size: contain

---

### Katuma

Visió i estratègia tot buscant la viabilitat

---

### Open Food Network

Visió i estratègia a llarg termini tot buscant la viabilitat

---

## Bibliografia

Cooperatives de consum agroecològic de plataforma. <br>
El paper de les Tecnologies de la Informació i la Comunicació en el consum cooperatiu de productes agroecològics.

*Ricard Espelt*

[www.cooperatives.barcelona](http://www.cooperatives.barcelona/)

---

class: impact

## Necessitem més mans!

---

class: impact

## Uni-vos a la xarxa!

---

## Enllaços

https://speakerdeck.com/coopdevs
https://github.com/openfoodfoundation/openfoodnetwork/
https://openfoodnetwork.org/slack-invite

---

class: impact

background-image: radial-gradient(rgba(58, 64, 122, 0.75), rgba(58, 64, 122, 0.85)), url(img/preguntes.jpg)

## Preguntes

---

---
